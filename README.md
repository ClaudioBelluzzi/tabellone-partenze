# Tabellone partenze

## Cos'è questo progetto

Questo è un semplice programma in grado di riprodurre un tabellone delle partenze dei treni presente in moltissime stazioni della rete italiana. È stato realizzato in python e l'interfaccia grafica è stata realizzata con la libreria PySimpleGUI.

## Cosa prevederà

Questo programma sarà composto da 2 finestre. Nella prima si potrà controllare in tempo reale il tabellone tramite apposita interfaccia grafica. 
La seconda invece è il vero e proprio tabellone che permetterà di vedere tramite il suo caratteristico aspetto i dati inseriti nella prima finestra. Il tabellone è in grado di far scorrere il campo *destinazione* di ogni treno se troppo lungo e ha la possibilità di far lampeggiare il numero di binario se il treno è in partenza.

È stata caricata la versione del tabellone completa di tutte le funzionalità. Ora è presente anche il primo programma che permette di modificare tramite GUI i dati del tabellone. 
Per garantire il corretto funzionamento del tabellone è necessario aprire per primo il file eseguibile **controllo_tabellone.exe** e solo poi il file eseguibile **tabellone_partenze.exe**. 
È **necessario** che entrambi gli eseguibili siano nella stessa cartella.

Claudio Belluzzi 04/01/2021
