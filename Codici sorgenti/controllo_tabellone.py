import PySimpleGUI as sg

i = 0
ore = []
ore.append("")

while (i < 24):
    if (len(str(i)) < 2):
        ore.append("0" + str(i))
    else:
        ore.append(str(i))
    i += 1

minuti = []
minuti.append("")
i = 0

while (i < 60):
    if (len(str(i)) < 2):
        minuti.append("0" + str(i))
    else:
        minuti.append(str(i))
    i += 1

ritardi = []
ritardi.append("")
i = 5

while (i <= 360):
    ritardi.append(str(i) + "'")
    i += 5

#def scritturaFile():
    #cose

layout = [ [sg.Text("Controllo tabellone partenze", font=["", 20])],
           [sg.Text("Categoria    Numero      Destinazione          Ora           Rit         Bin       Lampeggio", font=["", 14])],
           [sg.InputText(font=["", 14], size=(8, 1), k="11"), sg.InputText(font=["", 14], size=(8, 1), k="12"), sg.InputText(font=["", 14], size=(14, 1), k="13"), sg.Combo(ore, font=["", 14], readonly=True, k="14"), sg.Combo(minuti, font=["", 14], readonly=True, k="15"), sg.Combo(ritardi, font=["", 14], readonly=True, k="16"), sg.InputText(font=["", 14], size=(4, 1), k="17"), sg.Radio("On", group_id=1, font=["", 14], k="18"), sg.Radio("Off", group_id=1, font=["", 14], default=True)],
           [sg.InputText(font=["", 14], size=(8, 1), k="21"), sg.InputText(font=["", 14], size=(8, 1), k="22"), sg.InputText(font=["", 14], size=(14, 1), k="23"), sg.Combo(ore, font=["", 14], readonly=True, k="24"), sg.Combo(minuti, font=["", 14], readonly=True, k="25"), sg.Combo(ritardi, font=["", 14], readonly=True, k="26"), sg.InputText(font=["", 14], size=(4, 1), k="27"), sg.Radio("On", group_id=2, font=["", 14], k="28"), sg.Radio("Off", group_id=2, font=["", 14], default=True)],
           [sg.InputText(font=["", 14], size=(8, 1), k="31"), sg.InputText(font=["", 14], size=(8, 1), k="32"), sg.InputText(font=["", 14], size=(14, 1), k="33"), sg.Combo(ore, font=["", 14], readonly=True, k="34"), sg.Combo(minuti, font=["", 14], readonly=True, k="35"), sg.Combo(ritardi, font=["", 14], readonly=True, k="36"), sg.InputText(font=["", 14], size=(4, 1), k="37"), sg.Radio("On", group_id=3, font=["", 14], k="38"), sg.Radio("Off", group_id=3, font=["", 14], default=True)],
           [sg.InputText(font=["", 14], size=(8, 1), k="41"), sg.InputText(font=["", 14], size=(8, 1), k="42"), sg.InputText(font=["", 14], size=(14, 1), k="43"), sg.Combo(ore, font=["", 14], readonly=True, k="44"), sg.Combo(minuti, font=["", 14], readonly=True, k="45"), sg.Combo(ritardi, font=["", 14], readonly=True, k="46"), sg.InputText(font=["", 14], size=(4, 1), k="47"), sg.Radio("On", group_id=4, font=["", 14], k="48"), sg.Radio("Off", group_id=4, font=["", 14], default=True)],
           [sg.InputText(font=["", 14], size=(8, 1), k="51"), sg.InputText(font=["", 14], size=(8, 1), k="52"), sg.InputText(font=["", 14], size=(14, 1), k="53"), sg.Combo(ore, font=["", 14], readonly=True, k="54"), sg.Combo(minuti, font=["", 14], readonly=True, k="55"), sg.Combo(ritardi, font=["", 14], readonly=True, k="56"), sg.InputText(font=["", 14], size=(4, 1), k="57"), sg.Radio("On", group_id=5, font=["", 14], k="58"), sg.Radio("Off", group_id=5, font=["", 14], default=True)],
           [sg.InputText(font=["", 14], size=(8, 1), k="61"), sg.InputText(font=["", 14], size=(8, 1), k="62"), sg.InputText(font=["", 14], size=(14, 1), k="63"), sg.Combo(ore, font=["", 14], readonly=True, k="64"), sg.Combo(minuti, font=["", 14], readonly=True, k="65"), sg.Combo(ritardi, font=["", 14], readonly=True, k="66"), sg.InputText(font=["", 14], size=(4, 1), k="67"), sg.Radio("On", group_id=6, font=["", 14], k="68"), sg.Radio("Off", group_id=6, font=["", 14], default=True)],
           [sg.InputText(font=["", 14], size=(8, 1), k="71"), sg.InputText(font=["", 14], size=(8, 1), k="72"), sg.InputText(font=["", 14], size=(14, 1), k="73"), sg.Combo(ore, font=["", 14], readonly=True, k="74"), sg.Combo(minuti, font=["", 14], readonly=True, k="75"), sg.Combo(ritardi, font=["", 14], readonly=True, k="76"), sg.InputText(font=["", 14], size=(4, 1), k="77"), sg.Radio("On", group_id=7, font=["", 14], k="78"), sg.Radio("Off", group_id=7, font=["", 14], default=True)],
           [sg.InputText(font=["", 14], size=(8, 1), k="81"), sg.InputText(font=["", 14], size=(8, 1), k="82"), sg.InputText(font=["", 14], size=(14, 1), k="83"), sg.Combo(ore, font=["", 14], readonly=True, k="84"), sg.Combo(minuti, font=["", 14], readonly=True, k="85"), sg.Combo(ritardi, font=["", 14], readonly=True, k="86"), sg.InputText(font=["", 14], size=(4, 1), k="87"), sg.Radio("On", group_id=8, font=["", 14], k="88"), sg.Radio("Off", group_id=8, font=["", 14], default=True)],
           [sg.InputText(font=["", 14], size=(8, 1), k="91"), sg.InputText(font=["", 14], size=(8, 1), k="92"), sg.InputText(font=["", 14], size=(14, 1), k="93"), sg.Combo(ore, font=["", 14], readonly=True, k="94"), sg.Combo(minuti, font=["", 14], readonly=True, k="95"), sg.Combo(ritardi, font=["", 14], readonly=True, k="96"), sg.InputText(font=["", 14], size=(4, 1), k="97"), sg.Radio("On", group_id=9, font=["", 14], k="98"), sg.Radio("Off", group_id=9, font=["", 14], default=True)],
           [sg.Text("Messaggio informativo: ", font=["", 14]), sg.InputText(font=["", 14], k="messaggio")],
           [sg.Button("Invia", font=["", 14])] ]

finestra = sg.Window("Controllo Tabellone v0.9", layout)

file = open("scambio_dati.txt", "w")

file.write(("######0\n" * 9) + "\n")
file.close()

while (True):
    event, values = finestra.read()

    if (event == sg.WIN_CLOSED):
        break

    if (event == "Invia"):
        file = open("scambio_dati.txt", "w")
        file.write("")
        file.close()
        file = open("scambio_dati.txt", "a")

        i = 1
        j = 1
        while (i < 10):
            j = 1
            while (j < 9):
                cella = str(i) + str(j)

                if (values[cella] is None):
                    stringa = ""
                else:
                    stringa = str(values[cella]).upper()

                if (stringa == "FALSE"):
                    stringa = "0"
                if (stringa == "TRUE"):
                    stringa = "1"

                if (j == 4 and len(stringa) > 0):
                    file.write(stringa + ":")
                elif (j != 8 and j != 4):
                    file.write(stringa + "#")
                else:
                    file.write(stringa)
                j += 1
            file.write("\n")
            i += 1

        file.write(str(values["messaggio"]).upper() + "\n")
        file.close()

finestra.close()

#Ideato e creato da Claudio Belluzzi
