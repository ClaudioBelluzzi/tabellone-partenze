import PySimpleGUI as sg
import datetime

def aggiornaRiga(riga_numero, k, riga, lampeggio):
    stringa = "".join(riga[0]) + "".join(riga[1])

    if (len(riga[2]) > 13):
        if (k + 13 < len(riga[2])):
            stringa += "".join(riga[2][k : k + 13])
            k += 1
        elif (k < len(riga[2])):
            stringa += "".join(riga[2][k : len(riga[2])] + riga[2][0 : 13 - len(riga[2][k : len(riga[2])])])
            k += 1
        else:
            stringa += "".join(riga[2][0 : 13])
            k = 1

    else:
        k = 0
        stringa += "".join(riga[2])

    stringa += "".join(riga[3]) + "".join(riga[4])

    if (lampeggio):
        stringa += "".join(riga[5])
    else:
        stringa += " "*5

    finestra[riga_numero].update(stringa)

    return k

def aggiungiSpazi(riga):
    riga[0] = riga[0][0 : 3] + list(" " * (4 - len(riga[0][0 : 3])))

    riga[1] = riga[1][0 : 5] + list(" " * (6 - len(riga[1][0 : 5])))

    if (len(riga[2]) <= 13):
        riga[2] = riga[2][0 : 13] + list(" " * (13 - len(riga[2][0 : 13])))
    else:
        riga[2] = riga[2] + list(" " * 7)

    riga[3] = list(" ") + riga[3][0 : 5] + list(" " * (7 - len(riga[3][0 : 5])))

    riga[4] = riga[4][0 : 4] + list(" " * (5 - len(riga[4][0 : 4])))

    riga[5] = riga[5][0 : 4] + list(" " * (5 - len(riga[5][0 : 4])))

    return riga

def unisciListe(lista):
    lunghezza = len(lista)
    nuovaLista = []

    i = 0
    j = 0
    while (i < lunghezza):
        j = 0
        while (j < len(lista[i])):
            nuovaLista.append(lista[i][j])
            j += 1
        i += 1
    return nuovaLista

def leggiFile():
    global righe, informazioni, counter, messaggio_informativo
    file = open("scambio_dati.txt")
    dati = file.read()

    i = 0
    counter = 0
    while (i < len(dati)):
        if (dati[i] == '\n'):
            counter += 1
        i += 1
    counter -= 1

    indiceInizio = 0
    indiceFine = 0

    i = 0

    while (i < counter and i < 9):
        while (dati[indiceFine] != "#"):
            indiceFine += 1
        informazioni.append(list(dati[indiceInizio : indiceFine]))

        j = 0
        while (j < 5):
            indiceInizio = indiceFine + 1
            indiceFine += 1
            while (dati[indiceFine] != "#"):
                indiceFine += 1
            informazioni.append(list(dati[indiceInizio : indiceFine]))
            j += 1

        indiceInizio = indiceFine + 1
        indiceFine += 1
        while (dati[indiceFine] != "\n"):
            indiceFine += 1
        informazioni.append(list(dati[indiceInizio : indiceFine]))

        righe.append(informazioni)

        i += 1
        indiceInizio = indiceFine + 1
        while (dati[indiceFine] != "\n"):
            indiceFine += 1
        informazioni = []

    indiceFine += 1
    while (dati[indiceFine] != "\n"):
        indiceFine += 1
    messaggio_informativo = list(dati[indiceInizio : indiceFine])

    file.close()

def messaggioInformativo(messaggio, k):
    if (len(messaggio) <= 41):
        messaggio += ((" ") * (41 - len(messaggio)))
        finestra["12"].update("".join(messaggio))
    else:
        messaggio += " " * 14
        if (k + 41 < len(messaggio)):
            finestra["12"].update("".join(messaggio[k : k + 41]))
            k += 1
        elif (k < len(messaggio)):
            finestra["12"].update("".join(messaggio[k : len(messaggio)] + messaggio[0 : 41 - len(messaggio[k : len(messaggio)])]))
            k += 1
        else:
            finestra["12"].update("".join(messaggio[0 : 41]))
            k = 1

    return k


#Costanti
dimensione1=70
dimensione2=40
font="liberationmono"
colore1="orange"
colore2="black"

#Definizione del layout della finestra
layout = [ [sg.Text("PARTENZE", font=[font, dimensione1], text_color=colore2, background_color=colore1, k="1")],
           [sg.Text("TRENO     DESTINAZIONE  ORARIO RIT  BIN  ", font=[font, dimensione2], text_color=colore2, background_color=colore1, k="2")],
           [sg.Text("                                         ", font=[font, dimensione2], text_color=colore1, background_color=colore2, k="3")],
           [sg.Text("                                         ", font=[font, dimensione2], text_color=colore1, background_color=colore2, k="4")],
           [sg.Text("                                         ", font=[font, dimensione2], text_color=colore1, background_color=colore2, k="5")],
           [sg.Text("                                         ", font=[font, dimensione2], text_color=colore1, background_color=colore2, k="6")],
           [sg.Text("                                         ", font=[font, dimensione2], text_color=colore1, background_color=colore2, k="7")],
           [sg.Text("                                         ", font=[font, dimensione2], text_color=colore1, background_color=colore2, k="8")],
           [sg.Text("                                         ", font=[font, dimensione2], text_color=colore1, background_color=colore2, k="9")],
           [sg.Text("                                         ", font=[font, dimensione2], text_color=colore1, background_color=colore2, k="10")],
           [sg.Text("                                         ", font=[font, dimensione2], text_color=colore1, background_color=colore2, k="11")],
           [sg.Text("                                         ", font=[font, dimensione2], text_color=colore2, background_color=colore1, k="12")] ]

#Creazione della finestra
finestra = sg.Window("Tabellone Partenze v0.9", layout, element_padding=(0, 0), margins=[0, 0])

counter = 0

i = True
j = False
indici = [0]*10
lampeggio_binario = False
messaggio_informativo = []

tdelta = datetime.timedelta(milliseconds=500)
time = datetime.datetime.now()

event, values = finestra.read(timeout=1)
finestra["1"].expand(expand_x=True)

while (True):
    event, values = finestra.read(timeout=1)

    if (event == sg.WIN_CLOSED):
        break

    if ((time + tdelta) < (datetime.datetime.now()) or i):
        righe = []
        informazioni = []
        leggiFile()
        i = 0
        while (i < counter):
            righe[i] = aggiungiSpazi(righe[i])
            if (int("".join(righe[i][6]))):
                indici[i] = aggiornaRiga(str(i + 3), indici[i], righe[i], lampeggio_binario)
            else:
                indici[i] = aggiornaRiga(str(i + 3), indici[i], righe[i], True)
            i += 1
        i = 0
        if (j):
            lampeggio_binario = not(lampeggio_binario)
            j = False
        else:
            j = True
        time = datetime.datetime.now()

        indici[9] = messaggioInformativo(messaggio_informativo, indici[9])

finestra.close()

#Ideato e creato da Claudio Belluzzi
